import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;

public class Assignment {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    int totalPrice = 0;

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to " + food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0){
            totalPrice += price;
            String currentText = textPane1.getText();
            textPane1.setText(currentText+food+price+"yen\n");
            totallabel.setText("Total:"+String.valueOf(totalPrice)+" yen");
            JOptionPane.showMessageDialog(
                    null,
                    "Order for "+ food + " recived");
        }
    }

    private JPanel root;
    private JButton spButton;
    private JButton fspButton;
    private JButton tykButton;
    private JButton grButton;
    private JButton gcButton;
    private JButton vpButton;
    private JTextArea textArea1;
    private JButton button1;
    private JTextPane textPane1;
    private JLabel totallabel;
    private JTextPane textPane2;

    public Assignment() {
        spButton.setIcon(new ImageIcon(this.getClass().getResource("Spring rolls.jpg")));
        fspButton.setIcon(new ImageIcon(this.getClass().getResource("Fried spring rolls.jpg")));
        tykButton.setIcon(new ImageIcon(this.getClass().getResource("Tom Yum Khun.jpg")));
        grButton.setIcon(new ImageIcon(this.getClass().getResource("Gapao Rice.jpg")));
        gcButton.setIcon(new ImageIcon(this.getClass().getResource("Green Curry.jpg")));
        vpButton.setIcon(new ImageIcon(this.getClass().getResource("Vietnam Pho.jpg")));

        spButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Spring Rolls ",450);
            }
        });
        fspButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried Spring Rolls ",500);
            }
        });
        tykButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tom Yum Khun ",700);
            }
        });
        grButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gapao Rice ",850);
            }
        });
        gcButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Green Curry ",800);
            }
        });
        vpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Vietnam Pho ",780);
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to check out?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you!! Total price is "+ totalPrice +" yen.");
                            textPane1.setText("");
                            totallabel.setText("Total: 0 yen");
                            totalPrice = 0;
                }
            }
        });
    }
}
